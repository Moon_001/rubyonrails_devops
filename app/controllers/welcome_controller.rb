class WelcomeController < ApplicationController
  def index
    render plain: 'hello-world', status: 200
  end
end
