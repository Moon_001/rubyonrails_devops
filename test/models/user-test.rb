require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "should have first_name, last_name, age, and email columns" do
    user = User.new(
      first_name: "toto",
      last_name: "titi",
      age: 30,
      email: "toto@example.com"
    )

    assert user.save
    saved_user = User.find(user.id)
    
    puts "Expected first_name: toto, Actual first_name: #{saved_user.first_name}"
    puts "Expected last_name: titi, Actual last_name: #{saved_user.last_name}"
    puts "Expected age: 30, Actual age: #{saved_user.age}"
    puts "Expected email: toto@example.com, Actual email: #{saved_user.email}"
    
    assert_equal "toto", saved_user.first_name
    assert_equal "titi", saved_user.last_name
    assert_equal 30, saved_user.age
    assert_equal "toto@example.com", saved_user.email
  end
end
