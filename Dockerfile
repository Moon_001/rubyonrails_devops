# Utilisez une image de base Rails
FROM ruby:3.0.4
# Répertoire de travail dans le conteneur
WORKDIR /app

# Copier les fichiers nécessaires dans le conteneur
COPY Gemfile Gemfile.lock /app/
COPY . /app/

# Installation des gems
RUN bundle install

# Exposer le port 3000
EXPOSE 3000

# Commande pour démarrer l'application
CMD ["rails", "server", "-b", "0.0.0.0"]
