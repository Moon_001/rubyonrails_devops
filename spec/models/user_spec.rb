# spec/models/user_spec.rb
require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { FactoryBot.create(:user) }

  it 'validates uniqueness of email' do
    existing_user = FactoryBot.create(:user, email: 'toto@test.com')
    user.email = 'toto@test.com'
    expect(user).not_to be_valid
    
  end

  it 'is valid with valid attributes' do
    expect(user).to be_valid
  end

  it 'is not valid without a first name' do
    user.first_name = nil
    expect(user).not_to be_valid
  end

  it 'is not valid without a last name' do
    user.last_name = nil
    expect(user).not_to be_valid
  end


  it 'is not valid without an email' do
    user.email = nil
    expect(user).not_to be_valid
  end

  it 'is not valid with an invalid email format' do
    user.email = 'invalid_email'
    expect(user).not_to be_valid
  end

  it 'is not valid with a duplicate email' do
    existing_user = FactoryBot.create(:user, email: 'duplicate@test.com')
    user.email = 'duplicate@test.com'
    expect(user).not_to be_valid
  end

  it 'is not valid with a short password' do
    user.password = 'short'
    expect(user).not_to be_valid
  end

  it 'is valid with a long password' do
    user.password = 'a' * 20
    expect(user).to be_valid
  end

end
