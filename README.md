# Ruby on Rails DevOps Project

This project involves creating a Ruby on Rails application with unit tests, a GitLab CI/CD pipeline, and deploying it to a local Kubernetes cluster using Helm.

## Prerequisites

- Ruby 2.7
- Rails 6.1
- Docker
- Kubernetes
- Helm
- GitLab Runner

## Installation

1. Clone this repository:

```bash
git clone https://github.com/your-username/rubyonrails_devops.git
cd rubyonrails_devops
```
2. Install Ruby dependencies and configure your database:

```bash
bundle install
rails db:create db:migrate
```
3. Start the Rails server:
```bash
rails server
```


### Unit Test
```bash
rails test test/models/user-test.rb
```

### Deploying to Kubernetes with Helm

1. Make sure your Kubernetes cluster is up and running, and Helm is configured to access it.

2. Navigate to the directory containing the Helm chart:
```bash
cd my-rails-chart
```
3. Install the application on the Kubernetes cluster using Helm:
```
helm install my-rails-app .
```


